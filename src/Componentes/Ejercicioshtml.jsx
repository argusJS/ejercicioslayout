import React from 'react';
import '../css/Ejercicios.css';

export default class Ejercicioshtml extends React.Component {



    render() {
        return (
            <>
                <div className="main">

                    <Ejercicio1 ejercicio={1} activado={this.props.activados[0]}></Ejercicio1>
                    <Ejercicio1 ejercicio={2} activado={this.props.activados[1]}></Ejercicio1>
                    <Ejercicio1 ejercicio={3} activado={this.props.activados[2]}></Ejercicio1>
                    <Ejercicio1 ejercicio={4} activado={this.props.activados[3]}></Ejercicio1>

                </div>
            </>
        );
    }
}

class Ejercicio1 extends React.Component {





    render() {


        const visible = () => {
            if (this.props.activado) return ("")
            else return ("ocultar")
        };

        return (
            <>
                <div className={`wrapper  ${visible()}`}>
                    <ContenidoController ejercicio={this.props.ejercicio}></ContenidoController>

                </div>

            </>
        );
    }
}




class ContenidoController extends React.Component {


    state = {
        cuerpo: false,
    }

    cuerpo = () => {

        if (this.state.cuerpo) return <Contenido activar={this.activar} ejercicio={this.props.ejercicio}></Contenido>;
        else if (!this.state.cuerpo) return (
            <>
                <Resumenes ejercicio={this.props.ejercicio}></Resumenes>
                <i class="fa fa-arrow-down flecha" aria-hidden="true" onClick={this.activar}></i>
            </>
        );
    }

    activar = () => {
        this.setState({

            cuerpo: !this.state.cuerpo,

        });
    }


    render() {


        return (

            <>
                <p className="titulo" onClick={this.activar}>{`Ejercicio ${this.props.ejercicio}`}</p>

                <div className="ejercicio-wrapper " onClick={this.activar}>


                    <div className="linea"></div>
                    <div className="ejercicio-contenido">

                        {this.cuerpo()}



                    </div>



                </div>





                {/* <Contenido ejercicio={this.props.ejercicio}></Contenido> */}
            </>
        );
    }
}

const Contenido = (props) => {
    if (props.ejercicio == 1) return (<Contenido1 activar={props.activar}></Contenido1>)
    else if (props.ejercicio == 2) return (<Contenido2 activar={props.activar}></Contenido2>);
    else if (props.ejercicio == 3) return (<Contenido1 activar={props.activar}></Contenido1>);
    else if (props.ejercicio == 4) return (<Contenido1 activar={props.activar}></Contenido1>);


}

const Resumenes = (props) => {
    if (props.ejercicio == 1) return (<Resumen1></Resumen1>)
    else if (props.ejercicio == 2) return (<Resumen2></Resumen2>);
    else if (props.ejercicio == 3) return (<Resumen3></Resumen3>);
    else if (props.ejercicio == 4) return (<Resumen4></Resumen4>);


}







const Resumen1 = () => {
    return (
        <>
            <p className="ejercicio-resumen">Resumen del ejercicio 1</p>
            
        </>
    );
}

const Resumen2 = () => {
    return (
        <>
            <p className="ejercicio-resumen">Resumen del ejercicio 2</p>
            
        </>
    );
}
const Resumen3 = () => {
    return (
        <>
            <p className="ejercicio-resumen">Resumen del ejercicio 3</p>
            
        </>
    );
}
const Resumen4 = () => {
    return (
        <>
            <p className="ejercicio-resumen">Resumen del ejercicio 4</p>
            
        </>
    );
}
const Contenido1 = (props) => {

    return (
        <>

            <p className="ejercicio-texto">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <i class="fa fa-arrow-up flecha" aria-hidden="true" onClick={props.activar}></i>
        </>
    );
}


const Contenido2 = (props) => {

    return (
        <>
            <p className="ejercicio-texto">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <i class="fa fa-arrow-up flecha" aria-hidden="true" onClick={props.activar}></i>
        </>
    );
}





