import React from "react";
import Home from './Componentes/Home.jsx';
import Ejercicios from './Componentes/Ejercicioshtml.jsx';
import './css/Ejercicios.css';




export default class App extends React.Component {


  state = {

    activados: [false, false],
    menu: false,
  }

  activarEjercicio = (id, activado) => {

    let ejercicios = this.state.activados.map((x) => x);
    ejercicios[id] = activado;

    this.setState({ activados: ejercicios });
    console.log(this.state);
  }

  activarMenu = () => {

    this.setState({
      menu: !this.state.menu,
    });
    console.log("pulsado" + this.state.menu);
  }



  render() {

    const menu = () => {
      if (!this.state.menu) return ("reducir")
      else if (this.state.menu) return ("menu");
    }



    return (
      <>
        <div className="principal">

          <div className="nav">
            <div className="menu-wrapper">

              <div className="menu-boton" onClick={this.activarMenu}></div>

            </div>
            <div className="nav-wrapper">

              <EjercicioBoton activado={this.state.activados[0]} activar={this.activarEjercicio} id={0} nombre="Ejercicio 1" ></EjercicioBoton>
              <EjercicioBoton activado={this.state.activados[1]} activar={this.activarEjercicio} id={1} nombre="Ejercicio 2" ></EjercicioBoton>
              <EjercicioBoton activado={this.state.activados[2]} activar={this.activarEjercicio} id={2} nombre="Ejercicio 3" ></EjercicioBoton>
              <EjercicioBoton activado={this.state.activados[3]} activar={this.activarEjercicio} id={3} nombre="Ejercicio 4" ></EjercicioBoton>

            </div>

            <div className="input-wrapper">

              <input className="search"></input>

            </div>

          </div>

          <div className="body-ejercicios">

            <div className={`${menu()}`}>
              <div className="linea-menu"></div>
              <EjercicioBoton activado={this.state.activados[0]} activar={this.activarEjercicio} id={0} nombre="Ejercicio 1" ></EjercicioBoton>
              <div className="linea-menu"></div>
              <EjercicioBoton activado={this.state.activados[1]} activar={this.activarEjercicio} id={1} nombre="Ejercicio 2" ></EjercicioBoton>
              <div className="linea-menu"></div>
              <EjercicioBoton activado={this.state.activados[2]} activar={this.activarEjercicio} id={2} nombre="Ejercicio 3" ></EjercicioBoton>
              <div className="linea-menu"></div>
              <EjercicioBoton activado={this.state.activados[3]} activar={this.activarEjercicio} id={3} nombre="Ejercicio 4" ></EjercicioBoton>
              <div className="linea-menu"></div>


            </div>
            <Ejercicios activados={this.state.activados}></Ejercicios>
          </div>


        </div>
      </>
    );
  }
}


class EjercicioBoton extends React.Component {

  state = {
    active: false,
    estilo: "desactivado",

  };

  presionar = () => {

    if (!this.props.activado) {
      this.setState({ active: true });
      this.setState({ estilo: "" });
      this.props.activar(this.props.id, true);
    }
    else if (this.props.activado) {
      this.setState({ active: false });
      this.setState({ estilo: "desactivado" });
      this.props.activar(this.props.id, false);
    }




  };

  render() {

    let ocultar = () => {

      if (!this.state.active) return ("desactivado");
      else return ("");
    };

    return (

      <p onClick={this.presionar} className={`p-ejercicios-movil ${ocultar()}`}>{this.props.nombre}</p>


    );
  }
}
